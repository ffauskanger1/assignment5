# Assignment 5:Frontend Development with React

## Description

This project is about creating a React application taking use of routing, context API, communicating with an API and react components.

## Component Tree Wireframe
https://gitlab.com/ffauskanger1/assignment5/-/blob/main/PlanningAssets/Lost_in_Translation.pdf

## Heroku project

https://ffsf-assignment5-app.herokuapp.com/

## How to start project locally
### npm install
### npm start to run local server

## Contributors

Fredrik Fauskanger and Sondre Fjelde
